global _start
;On considère le fichier contenant le personnel comme deja existant
;https://www.tutorialspoint.com/assembly_programming/assembly_file_management.htm

section .data
file: db "test.txt", 0
lenfile: equ $-file
out: dw 0
coma : db ','
space : db ' '
buffer : db 0



section .text
; Entrée
; eax : entier à convertir
; esi : pointeur vers un buffer
; Sortie
; eax = pointeur vers le premier caractère de la chaîne de caractères
; ecx, longueur de la chaîne de caractères générée
toString:
    add esi, 9
    mov byte[esi], 0
    mov ebx, 10
    xor ecx, ecx

    .while:
        xor edx, edx   ; Nettoyage avant la division de edx:eax par ebx
        div ebx        ; eax /= 10
        add dl, '0'    ; Convertion du reste en ASCII
        dec esi        ; On stocke les caractère à l'envers
        mov [esi], dl
        inc ecx
        test eax, eax
        jnz .while  ; On répètre jusqu'a ce que eax = 0

        mov eax, esi
        ret




_start:

    ;ouverture fichier
    mov eax, 5
    mov ebx, file
    mov ecx, 0x441            ;append/write/create if not existent
    mov edx, 0777o          ;la totale
    int  80h
    mov esi,eax

    cmp eax,0
    jbe end ;erreur ouverture

    mov  esi, eax

    ;ecriture fichier
    ;mov eax, 4
    ;mov ebx, esi
    ;mov ecx, msg
    ;mov edx, len
    ;int 80h

    cmp eax,0
    jbe close ;erreur ecriture


    ;fermer fichier
    mov eax, 6
    mov ebx, esi
    int 80h


    ;ouverture fichier
    mov eax, 5
    mov ebx, file
    mov ecx, 0            ;read only
    mov edx, 0777o          ;la totale
    int  80h
    mov esi,eax

    xor edi,edi


read_list :
    ;lecture
    mov eax, 3
    mov ebx, esi
    mov ecx, buffer
    mov edx, 1
    int 80h

    ;test si erreur
    cmp eax,0
    jbe end_of_file

    cmp byte [buffer], ','
    je inc_compt
    jmp read_list

inc_compt:
    inc edi
    jmp read_list

end_of_file:
    ; close the file
    mov eax, 6
    mov ebx, esi
    int  80h


    mov eax,edi
    xor edx,edx
    mov ebx,2
    div ebx
    add eax,1


    ;On perd des trucs ici
    mov esi, out
    call toString
    mov edi, eax
    mov esi,ecx

    ;ouverture fichier
    mov eax, 5
    mov ebx, file
    mov ecx, 0x401            ;append
    mov edx, 0777o          ;la totale
    int  80h

    cmp eax,0
    jbe end ;erreur ouverture

    ;écrit le numero (normalement)
    mov edx, esi
    mov ecx, edi
    mov ebx, eax
    mov esi,eax
    mov eax, 4
    int 80h

    ;ecriture ,
    mov eax, 4
    mov ebx, esi
    mov ecx, coma
    mov edx, 1
    int 80h


writing_loop :
    mov eax, 3 ;Lecture clavier
    mov ebx, 0
    mov ecx, buffer
    mov edx, 1
    int 80h


    cmp byte [buffer],' '
    je print_coma


    mov eax, 4 ; Print
    mov ebx, esi
    mov ecx, buffer
    mov edx, 1
    int 80h

    cmp byte [buffer],10
    je end
    jmp writing_loop

print_coma :
    mov eax, 4
    mov ebx, esi
    mov ecx, coma
    mov edx, 1
    int 80h
    jmp writing_loop

close:
    ; close the file
   mov eax, 6
   mov ebx, esi
   int  80h

   jmp end
end:
    mov eax, 1
    xor ebx, ebx
    int 80h