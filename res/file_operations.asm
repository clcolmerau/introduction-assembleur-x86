global _start

section .data

message: db "GWAK !", 10
filename: db "file.txt", 0
fd: dw 0
buffer: dw 0

section .text

_start:
    call open_wo
    call write
    call close

    call open_ro
    call read
    call print
    call close

    mov eax, 1
    xor ebx, ebx
    int 80h

open_wo:
    mov eax, 5
    mov ebx, filename
    mov ecx, 0101q
    mov edx, 0666q
    int 80h
    mov [fd], eax
    ret

open_ro:
    mov eax, 5
    mov ebx, filename
    mov ecx, 0102q
    mov edx, 0666q
    int 80h
    mov [fd], eax
    ret

write:
    mov eax, 4
    mov ebx, [fd]
    mov ecx, message
    mov edx, 7
    int 80h
    ret

read:
    mov eax, 3
    mov ebx, [fd]
    mov ecx, [buffer]
    mov edx, 7
    int 80h
    ret

print:
    mov eax, 4
    mov ebx, 1
    mov ecx, byte [buffer]
    mov edx, 7
    int 80h
    ret

close:
    mov eax, 6
    mov ebx, [fd]
    int 80h
    ret
