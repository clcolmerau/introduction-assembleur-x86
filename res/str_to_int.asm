global _start

section .data

buffer: db 64 dup(0)

section .text

_start:
    xor esi, esi
    .convert:
        ; Lecture de l'entrée standard
        mov eax, 3
        xor ebx, ebx
        mov ecx, buffer
        mov edx, 1
        int 80h

        ; Conversion en entier
        xor ebx, ebx
        mov bl, [buffer]
        cmp ebx, 10
        je .print

        sub ebx, 48
        imul esi, 10
        add esi, ebx
        jmp .convert

    ; Valeur de retour
    .print:
        mov eax, 1
        xor ebx, ebx
        int 80h
